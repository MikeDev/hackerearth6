import sys
from path import Path
import importlib
import pkg_resources

PROJECT_ROOT = Path(__file__).parent
SRC: Path = PROJECT_ROOT / 'src'


def update_pythonpath():
    sys.path.append(SRC)
    sys.path += list(SRC.walkdirs())
    sys.path.append(PROJECT_ROOT / 'costants')


def check_requirements():
    with open(PROJECT_ROOT / 'requirements.txt') as f:
        pkg_resources.require(f.read().splitlines())


def run_module():
    assert len(sys.argv) >= 2, 'You must specify the module to execute'
    cmd = sys.argv[1]
    module = importlib.import_module(cmd)
    try:
        module.main()
    except Exception as e:
        with open(PROJECT_ROOT / 'exceptions', mode='a+') as f:
            f.write(str(e) + '\n')
        raise e

update_pythonpath()
check_requirements()
run_module()