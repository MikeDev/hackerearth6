from constants.paths import DATA_RAW
import pandas as pd

def main():
    df_train = pd.read_csv(DATA_RAW / 'train.csv')
    df_test = pd.read_csv(DATA_RAW / 'test.csv')
    n_train = len(df_train)
    n_test = len(df_test)
    df_ownership = pd.read_csv(RAW_DATA_DIR / 'Building_Ownership_Use.csv')
    df_structure = pd.read_csv(RAW_DATA_DIR / 'Building_Structure.csv')
    df = pd.concat([df_train, df_test], copy=False).reset_index(drop=True)
    df = df.merge(df_ownership, how='left', on='building_id')
    df = df.merge(df_structure, how='left', on='building_id')
    df.set_index('building_id', inplace=True)
    df_train = df.iloc[:n_train, :].reset_index()
    df_test = df.iloc[n_train:, :].reset_index()
    df_test.drop(columns='damage_grade')
    df_train.to_feather(DATA_RAW / 'train.feather')
    df_test.to_feather(DATA_RAW / 'test.feather')