import pandas as pd
from adjust_types import AdjustTypes
from constants.dataset import TARGETVAR
from constants.paths import DATA_RAW, DATA_PROCESSED
from drop_correlated_columns import DropCorrelatedColumns
from extract_diff import ExtractDiff
from fill_na import FillNa
from add_id_cols_info import AddIdColsInfo
from categorical_interactions import CategoricalInteraction


TARGETMAP = {'Grade 1': 0, 'Grade 2': 1, 'Grade 3': 2, 'Grade 4': 3, 'Grade 5': 4}

def main():
    train, test = pd.read_feather(DATA_RAW / 'train.feather'), pd.read_feather(DATA_RAW / 'test.feather')
    train[TARGETVAR] = train[TARGETVAR].apply(TARGETMAP.get).astype('int8')
    train, test = DropCorrelatedColumns().fit_transform(train, test)
    train, test = ExtractDiff().fit_transform(train, test)
    train, test = AdjustTypes().fit_transform(train, test)
    train, test = FillNa().fit_transform(train, test)
    train, test = CategoricalInteraction().fit_transform(train, test)
    train, test = AddIdColsInfo().fit_transform(train, test)
    test.reset_index(inplace=True, drop=True)
    catcols = train.select_dtypes('category').columns.tolist()
    train.to_feather(DATA_PROCESSED / 'train_processed.feather')
    test.to_feather(DATA_PROCESSED / 'test_processed.feather')
    pd.get_dummies(train, columns=catcols).to_feather(DATA_PROCESSED / 'train_processed_ohe.feather')
    pd.get_dummies(test, columns=catcols).to_feather(DATA_PROCESSED / 'test_processed_ohe.feather')


if __name__ == '__main__':
    main()
