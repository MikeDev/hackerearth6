from abstract_df_transformer import AbstractDFTransformer
from itertools import combinations, product, chain
from constants.procs import cramerv
from constants.dataset import TARGETVAR


class CategoricalInteraction(AbstractDFTransformer):

    def __init__(self, threshold_correlation=0.1):
        super().__init__()
        self.threshold_correlation = threshold_correlation
        self.geotechnical_cols = ['has_geotechnical_risk', 'has_geotechnical_risk_fault_crack',
                                  'has_geotechnical_risk_flood', 'has_geotechnical_risk_land_settlement',
                                  'has_geotechnical_risk_landslide', 'has_geotechnical_risk_liquefaction',
                                  'has_geotechnical_risk_other', 'has_geotechnical_risk_rock_fall']
        self.repair_cols = ['has_repair_started']
        self.secondary_cols = ['has_secondary_use', 'has_secondary_use_agriculture',
                               'has_secondary_use_hotel', 'has_secondary_use_rental',
                               'has_secondary_use_institution', 'has_secondary_use_school',
                               'has_secondary_use_industry', 'has_secondary_use_health_post',
                               'has_secondary_use_gov_office', 'has_secondary_use_use_police',
                               'has_secondary_use_other']
        self.superstructure_cols = ['has_superstructure_adobe_mud',
                                    'has_superstructure_mud_mortar_stone', 'has_superstructure_stone_flag',
                                    'has_superstructure_cement_mortar_stone',
                                    'has_superstructure_mud_mortar_brick',
                                    'has_superstructure_cement_mortar_brick', 'has_superstructure_timber',
                                    'has_superstructure_bamboo', 'has_superstructure_rc_non_engineered',
                                    'has_superstructure_rc_engineered', 'has_superstructure_other']
        self.bool_cols = [self.geotechnical_cols, self.repair_cols,
                          self.secondary_cols, self.superstructure_cols]
        self.to_keep = []

    def transform_train(self, train):
        for bool_col in chain(self.bool_cols):
            train[bool_col] = train[bool_col].astype('bool')
        for cols1, cols2 in combinations(self.bool_cols, 2):
            for col1, col2 in product(cols1, cols2):
                and_col = f'{col1}_and_{col2}'
                train[and_col] = train[col1] & train[col2]
                if cramerv(train, cols=[and_col, TARGETVAR])[0, 1] > self.threshold_correlation:
                    self.to_keep.append((col1, col2))
                    train[and_col] = train[and_col].astype('category')
                else:
                    del train[and_col]
        for bool_col in chain(self.bool_cols):
            train[bool_col] = train[bool_col].astype('category')
        return train

    def transform_test(self, test):
        for bool_col in chain(self.bool_cols):
            test[bool_col] = test[bool_col].astype('bool')
        for col1, col2 in self.to_keep:
            and_col = f'{col1}_and_{col2}'
            test[and_col] = (test[col1] & test[col2]).astype('category')
        for bool_col in chain(self.bool_cols):
            test[bool_col] = test[bool_col].astype('category')
        return test
