import pandas as pd
from lightgbm import LGBMClassifier
from abstract_traintest_transformer import AbstractTrainTestTransformer
from constants.procs import fill_na_multiple


class FillNa(AbstractTrainTestTransformer):

    def transform(self, train_test: pd.DataFrame):
        train_test['count_families'] = train_test['count_families'].fillna(train_test['count_families'].dropna().mean())
        train_test = fill_na_multiple(train_test, ['has_repair_started'], model=LGBMClassifier())
        return train_test
