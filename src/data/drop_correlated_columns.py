from abstract_df_transformer import AbstractDFTransformer


class DropCorrelatedColumns(AbstractDFTransformer):

    def __init__(self):
        super().__init__()
        self.to_drop_cols = ['vdcmun_id',
                             'vdcmun_id_x', 'vdcmun_id_y',
                             'ward_id_x', 'ward_id_y',
                             'district_id', 'district_id_y']

    def transform_train(self, train):
        return train.drop(columns=self.to_drop_cols)

    def transform_test(self, test):
        return test.drop(columns=self.to_drop_cols)
