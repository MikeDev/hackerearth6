from abstract_df_transformer import AbstractDFTransformer
from operator import itemgetter
from constants.dataset import IDCOL


class AddIdColsInfo(AbstractDFTransformer):

    def __init__(self):
        super().__init__()
        self.drop_id = False

    def regularize_id(self, idcol, standard_len):
        return idcol.apply(lambda el: "0" * (standard_len - len(el)) + el if len(el) < standard_len else el)

    def add_cols_id(self, df):
        max_len = df[IDCOL].apply(len).max()
        fixed_idcol = self.regularize_id(df[IDCOL], max_len)
        for i in range(max_len):
            df[f'_ID_{i}'] = fixed_idcol.apply(itemgetter(i)).apply(lambda el: int(el, 16))
        return df

    def transform_train(self, train):
        train = self.add_cols_id(train)
        train['int_id'] = train[IDCOL].apply(lambda el: int(el, 16))
        return train

    def transform_test(self, test):
        test = self.add_cols_id(test)
        test['int_id'] = test[IDCOL].apply(lambda el: int(el, 16))
        return test