from abstract_df_transformer import AbstractDFTransformer


class ExtractDiff(AbstractDFTransformer):

    def transform_train(self, train):
        train['diff_floors'] = train['count_floors_post_eq'] - train['count_floors_pre_eq']
        train['height_ft_diff'] = train.height_ft_post_eq - train.height_ft_pre_eq
        return train

    def transform_test(self, test):
        test['diff_floors'] = test['count_floors_post_eq'] - test['count_floors_pre_eq']
        test['height_ft_diff'] = test.height_ft_post_eq - test.height_ft_pre_eq
        return test
