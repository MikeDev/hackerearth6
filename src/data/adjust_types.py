import pandas as pd

from abstract_traintest_transformer import AbstractTrainTestTransformer


class AdjustTypes(AbstractTrainTestTransformer):

    def transform(self, train_test: pd.DataFrame):
        bool_cols = train_test.columns[train_test.columns.str.startswith('has_')]
        train_test[bool_cols] = train_test[bool_cols].astype('category')
        strcols = train_test.select_dtypes(include='object').columns
        train_test[strcols] = train_test[strcols].astype('category')
        train_test[strcols] = train_test[strcols].apply(lambda col: col.cat.codes)
        train_test[strcols] = train_test[strcols].astype('category')
        intcols = train_test.select_dtypes(include='int64').columns
        for col in intcols:
            train_test[col] = pd.to_numeric(train_test[col], downcast='signed')
        return train_test