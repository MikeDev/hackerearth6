from random import sample
from typing import Union
from constants.procs import adversial_validation
import lightgbm as lgb
import numpy as np
import pandas as pd
from lightgbm import Dataset
from sklearn.model_selection import train_test_split


class AdversialRandomSearchLGBM:

    def __init__(self, objective: str, param_grid: dict, n_iterations: int,
                 on_train=False, metric='rmse', lower_is_better=True):
        self.lower_is_better = lower_is_better
        self.objective = objective
        self.metric = metric
        self.on_train = on_train
        self.n_iterations = n_iterations
        self.param_grid = param_grid
        self.past_combinations = []
        self.best_score_ = None
        self.best_params_ = None
        self.best_iteration_ = None

    def fit(self, X_train: pd.DataFrame, y_train: pd.DataFrame, X_test: pd.DataFrame,
            val_size=0.3, n_estimators=1000) -> 'AdversialRandomSearchLGBM':
        X_train = X_train.fillna(0)
        X_test = X_test.fillna(0)
        X_train, X_val = adversial_validation(X_train, X_test, perc_val=val_size, model=lgb.LGBMClassifier())
        y_train, y_val = y_train[X_train.index], y_train[X_val.index]
        for _ in range(self.n_iterations):
            combination = self._untried_params()
            params = {**dict(combination), 'objective': self.objective, 'metric': self.metric}
            gbm = lgb.train(params, Dataset(X_train, y_train),
                            valid_sets=[Dataset(X_val, y_val)], verbose_eval=False)
            score = gbm.best_score['valid_0'][str(self.metric)]
            iteration = gbm.best_iteration
            self.compare_with_bestscore(params, iteration, score)
            self.past_combinations.append(params)
        return self

    def compare_with_bestscore(self, params, iteration, score):
        if self.lower_is_better:
            score = - abs(score)
        if self.best_score_ is None or self.best_score_ < score:
            self.best_score_ = score
            self.best_params_ = params
            self.best_iteration_ = iteration

    def _untried_params(self):
        combination = list(self._get_combination(self.param_grid))
        while combination in self.past_combinations:
            combination = list(self._get_combination(self.param_grid))
        return combination

    def _get_combination(self, param_grid):
        for param, values in param_grid.items():
            if isinstance(values, str) or not hasattr(values, '__iter__'):
                yield param, values
            else:
                yield param, sample(list(values), 1)[0]
