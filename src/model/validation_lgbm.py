import numpy as np
import pandas as pd
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
import sys
from random_search_lgbm import RandomSearchLGBM
from argparse import ArgumentParser, ArgumentError


sys.path.append('../../')
import constants
from lightgbm import LGBMRegressor
from adversial_random_search_lgbm import AdversialRandomSearchLGBM
from utils import fixed_argv
import lightgbm as lgb
from constants.paths import DATA
from constants.dataset import IDCOL, TARGETVAR


OBJECTIVE = "multiclass"
param_grid = \
    {
        'max_depth': [3, 5, 10, 20, 50, 100],
        'min_data_in_leaf': np.arange(10, 511, 100),
        'learning_rate': [10e-4, 5e-4, 10e-3, 5e-3],
        'n_estimators': [3000, 4000],
        'min_gain_to_split': [0.0001, 0.001, 0.01, 0.1, 1],
        #'bagging_fraction': np.arange(0.5, 0.9, 0.1),
        'num_leaves': np.arange(1, 20) * 10,
        # 'drop_rate': [0.1, 0.3, 0.5],
        'lambda_l2': np.concatenate([np.arange(1, 5) / 100, np.array([10e-4])]),
        'early_stopping_round': 100,
        'boosting_type': 'goss',
        'n_jobs': -1,
        'verbose': -1,
        # 'bagging_freq': np.arange(1, 10) * 5,
        'num_classes': 5
    }


def grid_search_lgbm(trainpath: str, scoring: str):
    X, y = load_X_y(trainpath)
    train = lgb.Dataset(X, y)
    lgb.cv(param_grid, train, stratified=True, metrics='msle', early_stopping_rounds=100)
    r_cv = GridSearchCV(LGBMRegressor(n_jobs=-1, objective="regression", metric="rmse",
                                      boosting_type='dart'),
                        param_grid, cv=3, scoring=scoring, return_train_score=True)
    r_cv.fit(X=X, y=y)
    save_val_results(r_cv, scoring)


def random_search_lgbm(niters: int, trainpath: str, scoring: str):
    X, y = load_X_y(trainpath)
    rs = RandomSearchLGBM(objective=OBJECTIVE, metric=scoring, param_grid=param_grid,
                          n_iterations=niters, lower_is_better=False)
    rs.fit(X=X, y=y)
    save_val_results(rs, scoring)


def adversial_random_search_lgbm(niters: int, trainpath: str, testpath: str, scoring: str):
    X_train, y_train = load_X_y(trainpath)
    X_test = pd.read_feather(DATA / testpath)
    if IDCOL in X_test.columns: X_test.drop(columns=IDCOL, inplace=True)
    rs = AdversialRandomSearchLGBM(objective=OBJECTIVE, metric=scoring, param_grid=param_grid, n_iterations=niters)
    rs.fit(X_train, y_train, X_test, n_estimators=500)
    save_val_results(rs, scoring)


def save_val_results(r_cv, scoring):
    with open(constants.paths.MODELS / 'lgbm_best_params', mode='w') as f:
        f.write(repr(r_cv.best_params_))
    with open(constants.paths.MODELS / 'lgbm_best_score', mode='w') as f:
        f.write(scoring + ' : ' + repr(r_cv.best_score_))
    if hasattr(r_cv, 'best_iteration_'):
        with open(constants.paths.MODELS / 'lgbm_best_iteration', mode='w') as f:
            f.write(repr(r_cv.best_iteration_))


def load_X_y(trainpath):
    extension = trainpath.split('.')[-1]
    load_func = getattr(pd, 'read_' + extension)
    train = load_func(constants.paths.DATA / trainpath)
    if IDCOL in train.columns:
        train.drop(columns=IDCOL, inplace=True)
    X, y = train.drop(columns=[TARGETVAR]), train[TARGETVAR]
    return X, y


def main():
    args = get_input_args()
    trainpath = args.train
    if args.type == 'random':
        random_search_lgbm(args.n_iterations, trainpath, args.scoring)
    elif args.type == 'grid':
        grid_search_lgbm(trainpath, args.scoring)
    elif args.type == 'adversial-random':
        adversial_random_search_lgbm(args.n_iterations, trainpath, args.test, args.scoring)
    else:
        raise ArgumentError('Arguments not recognized')


def get_input_args():
    argparser = ArgumentParser()
    argparser.add_argument('-t', '--type', type=str, default='random',
                           help='type of param search. Valid values are ["random", "grid", "adversial-random"]')
    argparser.add_argument('-n', '--n-iterations', type=int, default=100
                           , help='number of iteration of random search')
    argparser.add_argument('-s', '--scoring', type=str, default='multi_logloss',
                           help='scoring function for validation')
    argparser.add_argument('--train', type=str, default='raw/train.csv',
                           help='relative path under data folder of the'
                                'train dataset')
    argparser.add_argument('--test', type=str, default='raw/test.csv',
                           help='relative path under data folder of the'
                                'test dataset')
    args = argparser.parse_args(fixed_argv(__file__))
    print(vars(args))
    return args


if __name__ == '__main__':
    main()
