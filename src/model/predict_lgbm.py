import sys
from operator import itemgetter

from sklearn.ensemble import RandomForestClassifier

sys.path.append('../../')
from lightgbm import LGBMRegressor, LGBMClassifier
import lightgbm as lgb
import pandas as pd
from constants.paths import DATA_PROCESSED, DATA_PROCESSING, OUTPUT, DATA_RAW, MODELS
from constants.dataset import TARGETVAR, IDCOL
from constants.procs import adversial_validation, ClusterValuePrediction
from sklearn.metrics import mean_squared_log_error
import numpy as np
from utils import fixed_argv
from constants.procs import str_currtime
from argparse import ArgumentParser
import json

# TODO move hyper params in a json file

TARGETMAP = {0: 'Grade 1', 1: 'Grade 2', 2: 'Grade 3', 3: 'Grade 4', 4: 'Grade 5'}
# PARAMS = {'max_depth': 3, 'min_data_in_leaf': 10, 'learning_rate': 0.0005, 'max_bin': 200, 'min_gain_to_split': 0.001, 'num_leaves': 180, 'lambda_l2': 0.001, 'boosting_type': 'gbdt', 'n_jobs': -1, 'verbose': -1, 'num_classes': 5, 'device_type': 'cpu', 'objective': 'multiclass', 'metric': 'multi_logloss'}
# PARAMS = {'max_depth': 10, 'min_data_in_leaf': 10, 'learning_rate': 0.0005, 'max_bin': 30, 'min_gain_to_split': 0.0001, 'num_leaves': 140, 'lambda_l2': 0.04, 'boosting_type': 'gbdt', 'n_jobs': -1, 'verbose': -1, 'num_classes': 5, 'device_type': 'cpu', 'objective': 'multiclass', 'metric': 'multi_logloss'}
PARAMS = {'min_split_gain': 1e-6, 'num_leaves': 1000}

from operator import itemgetter


def regularize_id(idcol, standard_len):
    return idcol.apply(lambda el: "0" * (standard_len - len(el)) + el if len(el) < standard_len else el)


def add_cols_id(df):
    max_len = df[IDCOL].apply(len).max()
    fixed_idcol = regularize_id(df[IDCOL], max_len)
    for i in range(max_len):
        df[f'_ID_{i}'] = fixed_idcol.apply(itemgetter(i)).apply(lambda el: int(el, 16))
    return df


def prob_to_y(probs):
    bestclass = max(enumerate(probs), key=itemgetter(1))[0]
    return TARGETMAP[bestclass]


def predict_and_save(trainname: str, testname: str):
    train = pd.read_feather(DATA_PROCESSED / trainname)
    test = pd.read_feather(DATA_PROCESSED / testname)
    train, test = add_cols_id(train), add_cols_id(test)
    if IDCOL in train.columns: train.drop(columns=IDCOL, inplace=True)
    # X_train, y_train = train.drop(columns=[TARGETVAR]), train[TARGETVAR].values
    if IDCOL in test.columns:
        sub = test[[IDCOL]]
        test.drop(columns=IDCOL, inplace=True)
    else:
        sub = pd.read_csv(DATA_RAW / 'test.csv', usecols=[IDCOL])
    # ypred = predict(X_train, y_train, test)
    ypred = predict(train.drop(columns=TARGETVAR), train[TARGETVAR], test)
    sub[TARGETVAR] = ypred
    sub.to_csv(OUTPUT / f'lgbm_cluster_{str_currtime()}.csv', index=False)


def predict(X_train, y_train, X_test):
    # with open(MODELS / 'lgbm_best_params') as f:
    #  params = json.load(f)
    # with open(MODELS / 'lgbm_best_iteration') as f:
    #     best_iter = int(f.read())
    boolcols = X_train.select_dtypes('bool').columns
    X_train[boolcols] = X_train[boolcols].apply(lambda col: col.astype('category'))
    catcols = X_train.select_dtypes('category').columns.tolist()
    boolcols = X_test.select_dtypes('bool').columns
    X_train[boolcols] = X_test[boolcols].apply(lambda col: col.astype('category'))
    catcols = X_test.select_dtypes('category').columns.tolist()
    lgbm1 = LGBMClassifier(**PARAMS, num_boost_round=1000)
    lgbm1.fit(X_train, y_train, categorical_feature=catcols)
    yhat = lgbm1.predict(X_test).tolist()
    return list(map(TARGETMAP.get, yhat))


def predict_rf(X_train, y_train, X_test):
    catcols = X_test.select_dtypes('category').columns
    train_len = len(X_train)
    train_test = pd.concat([X_train, X_test], copy=False)
    train_test.reset_index(inplace=True, drop=True)
    train_test[catcols] = train_test[catcols].apply(lambda col: col.cat.codes)
    X_train, X_test = train_test.iloc[:train_len], train_test.iloc[train_len:]
    X_test.reset_index(inplace=True, drop=True)
    del train_test, train_len
    rf = RandomForestClassifier(n_estimators=1000, n_jobs=-1)
    rf.fit(X_train, y_train)
    return list(map(TARGETMAP.get, rf.predict(X_test)))


def predict_clustering(train: pd.DataFrame, test: pd.DataFrame):
    clustering = ClusterValuePrediction(lambda i: LGBMClassifier(**PARAMS), n_clusters=5)
    clustering.fit(train, test)
    yhat = clustering.predict(test)
    print(yhat)
    return list(map(TARGETMAP.get, yhat.ravel()))


def main():
    argparser = ArgumentParser()
    argparser.add_argument('--train', type=str, default='train.csv',
                           help='train filename into data/processed folder')
    argparser.add_argument('--test', type=str, default='test.csv',
                           help='test filename into data/processed folder')
    args = argparser.parse_args(fixed_argv(__file__))
    predict_and_save(args.train, args.test)
