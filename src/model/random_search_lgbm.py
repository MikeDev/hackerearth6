from random import sample
from typing import Union

import lightgbm as lgb
import numpy as np
import pandas as pd
from lightgbm import Dataset
from sklearn.model_selection import train_test_split


class RandomSearchLGBM:

    def __init__(self, objective: str, param_grid: dict, n_iterations: int,
                 on_train=False, metric='rmse', lower_is_better=True):
        self.lower_is_better = lower_is_better
        self.objective = objective
        self.metric = metric
        self.on_train = on_train
        self.n_iterations = n_iterations
        self.param_grid = param_grid
        self.past_combinations = []
        self.best_score_ = None
        self.best_params_ = None
        self.best_iteration_ = None

    def fit(self, X: Union[pd.DataFrame, np.ndarray], y: Union[pd.Series, np.array],
            test_size=0.3, cv=1, n_estimators=1000) -> 'RandomSearchLGBM':
        for _ in range(self.n_iterations):
            combination = self._untried_params()
            scores = np.array([0] * cv, dtype='float')
            iterations = np.array([0] * cv, dtype='int')
            params = {**dict(combination), 'objective': self.objective, 'metric': self.metric}
            for i in range(cv):
                X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=test_size)
                gbm = lgb.train(params, Dataset(X_train, y_train),
                                valid_sets=[Dataset(X_val, y_val)], verbose_eval=False,
                                num_boost_round=n_estimators)
                scores[i] = gbm.best_score['valid_0'][str(self.metric)]
                iterations[i] = gbm.best_iteration
            self.compare_with_bestscore(params, iterations, scores)
            self.past_combinations.append(params)
        return self


    def compare_with_bestscore(self, combination, iterations, scores):
        meanscore = scores.mean()
        if self.lower_is_better:
            meanscore = - abs(meanscore)
        if self.best_score_ is None or self.best_score_ < meanscore:
            self.best_score_ = meanscore
            self.best_params_ = combination
            self.best_iteration_ = int(iterations.mean().round())

    def _untried_params(self):
        combination = list(self._get_combination(self.param_grid))
        while combination in self.past_combinations:
            combination = list(self._get_combination(self.param_grid))
        return combination

    def _get_combination(self, param_grid):
        for param, values in param_grid.items():
            if isinstance(values, str) or not hasattr(values, '__iter__'):
                yield param, values
            else:
                yield param, sample(list(values), 1)[0]
