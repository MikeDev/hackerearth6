import numpy as np
from scipy.special import erfinv
from pandas import DataFrame


class GaussRankScaler():

    def __init__(self, corrected = True):
        self.epsilon = 0.001
        self.lower = -1 + self.epsilon
        self.upper = 1 - self.epsilon
        self.range = self.upper - self.lower
        self.corrected = corrected

    def fit_transform(self, X):

        if isinstance(X, DataFrame):
            X = X.values

        if self.corrected:
            unique_mask = []
            for i in range(X.shape[1]):
                unique_mask.append(
                    np.unique(X[:, i], return_inverse=True)[1]
                )

        i = np.argsort(X, axis=0)
        j = np.argsort(i, axis=0)

        assert (j.min() == 0).all()
        assert (j.max() == len(j) - 1).all()

        j_range = len(j) - 1
        divider = j_range / self.range

        transformed = j / divider
        transformed = transformed - self.upper
        transformed = erfinv(transformed)

        if self.corrected:
            x_corrected = np.zeros_like(transformed, dtype=transformed.dtype)
            for i, m in enumerate(unique_mask):

                values = np.unique(m)

                for v in values:
                    c = transformed[:, i]
                    mean = c[m == v].mean()

                    x_corrected[:, i][m == v] = mean

            transformed = x_corrected

        return transformed